export type NightmareItem = {
  gvgSkillDetail: string;
  gvgSkillSP: number;
  questSkillDur: number;
  gvgSkillEN: string;
  questSkill: string;
  maxLv: number;
  uniqueID: number;
  addMDef: number;
  addMAtk: number;
  addPDef: number;
  icon: number;
  nameEN: string;
  attribute: number;
  maxPAtk: number;
  questSkillLead: number;
  maxPDef: number;
  id: number;
  gvgSkillDur: number;
  questSkillEN: string;
  gvgSkillLead: number;
  addPAtk: number;
  rarity: number;
  maxMDef: number;
  name: string;
  questSkillSP: number;
  questSkillDetail: string;
  gvgSkill: string;
  maxMAtk: number;
  global: boolean;
  image: string;
};

class NightmareService {
  async loadNightmares(): Promise<NightmareItem[]> {
    return (await fetch('data/nightmares.json')).json();
  }
}

export default new NightmareService();
