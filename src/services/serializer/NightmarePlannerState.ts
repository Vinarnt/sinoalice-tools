import { TimelineItem } from 'vis-timeline/types';

type NightmarePlannerState = {
  items: TimelineItem[];
};

export default NightmarePlannerState;
