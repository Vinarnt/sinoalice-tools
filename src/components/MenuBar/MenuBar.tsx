import { Link } from 'react-router-dom';
import React, { ReactElement } from 'react';

import './MenuBar.css';

function MenuBar(): ReactElement {
  return (
    <nav className="menu-bar">
      <ul>
        <Link to="/nightmare-planner">
          <li>Nightmare Planner</li>
        </Link>
      </ul>
    </nav>
  );
}

export default MenuBar;
