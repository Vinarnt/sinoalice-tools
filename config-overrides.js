const { addWebpackAlias, babelInclude, override } = require('customize-cra');
const path = require('path');

module.exports = override(
  babelInclude([path.resolve('src'), path.resolve('node_modules/vis-data')]),
  addWebpackAlias('assets', path.resolve('./public'))
);
