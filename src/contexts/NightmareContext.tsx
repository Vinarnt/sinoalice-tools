import { createContext } from 'react';
import { NightmareItem } from '../services/NightmareService';

export interface NightmareContextType {
  nightmares: NightmareItem[];
}

const NightmareContext = createContext<NightmareContextType>({
  nightmares: []
});

export default NightmareContext;
