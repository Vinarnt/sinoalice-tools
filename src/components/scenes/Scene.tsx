import React, { ReactElement, ReactNode } from 'react';
import PropTypes from 'prop-types';

import './scene.css';

type Props = {
  title: string;
  children: ReactNode;
};

export function Scene({ title, children }: Props): ReactElement {
  return (
    <div className="scene-container">
      <h1 className="scene-title">{title}</h1>
      {children}
    </div>
  );
}

Scene.propTypes = {
  title: PropTypes.string.isRequired
};
