import React, { ReactElement, useContext, useRef, useState } from 'react';
import { FaClipboard } from 'react-icons/fa';

import NightmarePlannerContext from '../../../../../contexts/NightmarePlannerContext';
import SharingContext from '../../../../../contexts/SharingContext';
import NightmarePlannerState from '../../../../../services/serializer/NightmarePlannerState';
import StateSerializer from '../../../../../services/serializer/StateSerializer';
import SharingService from '../../../../../services/SharingService';
import Tooltip from '../../../../Tooltip/Tooltip';

import './ShareTab.css';

export default function ShareTab(): ReactElement {
  const generatedLinkInputRef = useRef(null);
  const nightmarePlannerContext = useContext(NightmarePlannerContext);
  const { sharingLink, setSharingLink } = useContext(SharingContext);
  const [isGenerating, setGenerating] = useState(false);

  const handleGenerateLinkClick = async () => {
    setGenerating(true);
    const data = StateSerializer.serialize<NightmarePlannerState>({
      items:
        nightmarePlannerContext.timelineRef?.current?.items
          .get()
          .filter((o) => 'nightmare' in o) || []
    });

    setSharingLink(
      await SharingService.getSharingLink(
        `${window.location.href}?import=${data}`
      )
    );
    setGenerating(false);
  };

  const handleClipboardClick = () => {
    (generatedLinkInputRef.current as any)?.select();
    document.execCommand('copy');
  };

  return (
    <div className="share-link-container">
      {!sharingLink && (
        <div>
          <button disabled={isGenerating} onClick={handleGenerateLinkClick}>
            {isGenerating ? 'Generating link ...' : 'Generate link'}
          </button>
        </div>
      )}
      {sharingLink && (
        <div className="share-link-input-container">
          <input ref={generatedLinkInputRef} value={sharingLink} readOnly />
          <Tooltip id="clipboard-tooltip" effect="solid" place="top">
            Copy to clipboard
          </Tooltip>
          <button
            data-tip
            data-for="clipboard-tooltip"
            onClick={handleClipboardClick}
          >
            <FaClipboard />
          </button>
        </div>
      )}
    </div>
  );
}
