import React, { ReactElement } from 'react';
import {
  HashRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom';

import './App.css';
import NightmarePlanner from './components/scenes/NightmarePlanner/NightmarePlanner';
import MenuBar from './components/MenuBar/MenuBar';
import { NightmarePlannerProvider } from './contexts/NightmarePlannerContext';
import { SharingProvider } from './contexts/SharingContext';

function App(): ReactElement {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <MenuBar />
        </header>

        <main className="App-main">
          <Switch>
            <Redirect path="/" to="/nightmare-planner" exact></Redirect>
            <Route path="/nightmare-planner">
              <NightmarePlannerProvider>
                <SharingProvider>
                  <NightmarePlanner />
                </SharingProvider>
              </NightmarePlannerProvider>
            </Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
