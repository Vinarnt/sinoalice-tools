import React, { createContext, useState } from 'react';

export interface SharingContextType {
  sharingLink: string | null;
  setSharingLink: (link: string | null) => void;
}

const SharingContext = createContext<SharingContextType>({
  sharingLink: null,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setSharingLink: () => {}
});

export const SharingConsumer = SharingContext.Consumer;

type ProviderProps = {
  children: React.ReactNode;
};
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const SharingProvider = function ({ children }: ProviderProps) {
  const [sharingLink, setSharingLink] = useState<string | null>(null);
  return (
    <SharingContext.Provider
      value={{
        sharingLink,
        setSharingLink
      }}
    >
      {children}
    </SharingContext.Provider>
  );
};

export default SharingContext;
