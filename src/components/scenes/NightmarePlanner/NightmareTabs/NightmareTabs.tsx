import React, { ReactElement, useContext } from 'react';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

import NightmareContext, {
  NightmareContextType
} from '../../../../contexts/NightmareContext';
import DraggableNightmareItem from '../DraggableNightmareItem/DraggableNightmareItem';
import './NightmareTabs.css';
import ShareTab from './ShareTab/ShareTab';

export default function NightmareTabs(): ReactElement {
  const nightmareContext = useContext(NightmareContext);

  const getNightmareItems = (
    context: NightmareContextType,
    ...skillContains: string[]
  ): ReactElement[] => {
    return context.nightmares
      .filter((nightmare) =>
        skillContains.some((skillContain: string) =>
          nightmare.gvgSkillEN.includes(skillContain)
        )
      )
      .map((nightmare, index) => (
        <DraggableNightmareItem
          key={index}
          nightmare={nightmare}
        ></DraggableNightmareItem>
      ));
  };

  return (
    <Tabs>
      <TabList>
        <Tab>Buff</Tab>
        <Tab>Debuff</Tab>
        <Tab>Elemental</Tab>
        <Tab>SP</Tab>
        <Tab>Support Skill</Tab>
        <Tab>Comeback</Tab>
        <Tab>All</Tab>
        <Tab>Share</Tab>
      </TabList>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(
            nightmareContext,
            'Armor',
            'Blessed',
            'False Blade',
            'Song',
            'Melody'
          )}
        </div>
      </TabPanel>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(
            nightmareContext,
            'Cry of',
            'Armor of Lamentation',
            'Begrugding Blade'
          )}
        </div>
      </TabPanel>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(nightmareContext, 'Blessing', 'For Whom The Bell')}
        </div>
      </TabPanel>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(nightmareContext, 'Fairy Magic', 'King Magic')}
        </div>
      </TabPanel>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(nightmareContext, 'Chant', 'Wail')}
        </div>
      </TabPanel>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(nightmareContext, 'Roar')}
        </div>
      </TabPanel>
      <TabPanel>
        <div className="nightmare-items-container">
          {getNightmareItems(nightmareContext, '')}
        </div>
      </TabPanel>
      <TabPanel>
        <ShareTab></ShareTab>
      </TabPanel>
    </Tabs>
  );
}
