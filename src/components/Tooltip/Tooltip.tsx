import React, { ReactElement, ReactPortal, RefObject } from 'react';
import ReactDOM from 'react-dom';
import ReactTooltip, { TooltipProps } from 'react-tooltip';

type PortalProps = {
  children?: ReactElement;
};
function Portal({ children }: PortalProps): ReactPortal {
  return ReactDOM.createPortal(children, document.body);
}

type BaseTooltipProps = {
  ref?: RefObject<ReactTooltip>;
};
function Tooltip(props: TooltipProps & BaseTooltipProps): ReactElement {
  return (
    <Portal>
      <ReactTooltip {...props} />
    </Portal>
  );
}

export default Tooltip;
