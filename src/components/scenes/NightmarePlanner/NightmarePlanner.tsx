import ReactDOM from 'react-dom';
import React, {
  ReactElement,
  ReactNode,
  useContext,
  useEffect,
  useState
} from 'react';
import moment, { Moment } from 'moment';
import Timeline from 'react-vis-timeline';
import {
  TimelineOptions,
  TimelineItem,
  TimelineOptionsTemplateFunction,
  TimelineEventPropertiesResult
} from 'vis-timeline/types';
import { useHistory } from 'react-router';

import './NightmarePlanner.css';
import { Scene } from '../Scene';
import NightmareTabs from './NightmareTabs/NightmareTabs';
import NightmareService, {
  NightmareItem as NightmareItemData
} from '../../../services/NightmareService';
import NightmareContext from '../../../contexts/NightmareContext';
import NightmareItem, { TimelineDataItem } from './NightmareItem/NightmareItem';
import NightmarePlannerContext from '../../../contexts/NightmarePlannerContext';
import useQuery from '../../../hooks/useQuery';
import StateSerializer from '../../../services/serializer/StateSerializer';
import NightmarePlannerState from '../../../services/serializer/NightmarePlannerState';
import Spinner from '../../spinner/Spinner';
import SharingContext from '../../../contexts/SharingContext';

// Don't rerender timeline when context change
const MemoizedTimeline = React.memo(Timeline, () => {
  return true;
});

function NightmarePlanner(): ReactElement {
  const [nightmares, setNightmares] = useState<NightmareItemData[]>([]);
  const [isLoading, setLoading] = useState(true);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [isImporting, setImporting] = useState(false);
  const nightmarePlannerContext = useContext(NightmarePlannerContext);
  const sharingContext = useContext(SharingContext);
  const query = useQuery();
  const history = useHistory();
  let initialItems: TimelineItem[] | undefined;
  let timelineReady = !!nightmarePlannerContext.timelineRef?.current;

  const newMoment = (): Moment => {
    return moment.unix(0);
  };

  useEffect(() => {
    (async function asyncLoad() {
      console.log('Loading nightmares...');
      await setNightmares(await NightmareService.loadNightmares());
      console.log('Nightmares loaded');

      setLoading(false);
    })();
  }, []);

  useEffect(() => {
    if (isLoading) return;

    const timelineRef = nightmarePlannerContext.timelineRef;
    const timeline = timelineRef?.current;

    timeline?.items.update([
      {
        id: 'demon-one-background',
        content: '',
        start: newMoment().minutes(14).toDate(),
        end: newMoment().minutes(16).toDate(),
        type: 'background',
        className: 'demon-background'
      },
      {
        id: 'demon-two-background',
        content: '',
        start: newMoment().minutes(5).toDate(),
        end: newMoment().minutes(7).toDate(),
        type: 'background',
        className: 'demon-background'
      }
    ]);

    try {
      // Prevent hot reload to set custome times twice
      timeline?.timeline?.addCustomTime(
        newMoment().minutes(16).toDate(),
        'demon1'
      );
      (timeline?.timeline as any).setCustomTimeMarker('First demon', 'demon1');
      timeline?.timeline?.setCustomTimeTitle('', 'demon1');
      timeline?.timeline?.addCustomTime(
        newMoment().minutes(7).toDate(),
        'demon2'
      );
      (timeline?.timeline as any).setCustomTimeMarker('Second demon', 'demon2');
      timeline?.timeline?.setCustomTimeTitle('', 'demon2');
    } catch (e) {
      //ignore
    }
  }, [isLoading]);

  useEffect(() => {
    if (isLoading) return;
    // Load state
    const importData = query.get('import');
    if (importData) {
      setImporting(true);

      const timelineRef = nightmarePlannerContext.timelineRef;
      const timeline = timelineRef?.current;

      console.log('Import data');

      query.delete('import');
      history.replace({ search: query.toString() });
      const state = StateSerializer.deserialize<NightmarePlannerState>(
        importData
      );
      timeline?.items
        .get()
        .filter((item) => 'nightmare' in item)
        .forEach((item) => timeline.items.remove(item));

      if (timelineReady) {
        setTimeout(() => {
          timeline?.items.add(state.items);
        }, 0);
      } else {
        initialItems = state.items;
      }
      console.log('Data imported');
      setImporting(false);
    }
  });

  const resetSharingLink = () => {
    sharingContext.setSharingLink(null);
  };

  const handleItemMoving = (item: TimelineItem, callback: any) => {
    const timelineRef = nightmarePlannerContext.timelineRef;
    if (!timelineRef?.current) return;

    resetSharingLink();

    const itemIds = timelineRef.current.timeline.getVisibleItems();
    const items = timelineRef.current.items.get(itemIds);
    const {
      nightmare: { gvgSkillDur, gvgSkillLead }
    } = item as TimelineDataItem;

    const closestItem = items
      ?.filter(
        (otherItem: TimelineItem) =>
          otherItem.id != item.id && (otherItem.end ?? 0) < item.start
      )
      ?.sort(
        (a: TimelineItem, b: TimelineItem) =>
          (b.end as number) - (a.start as number)
      )?.[0];

    if (closestItem) {
      const { start, end } = timelineRef.current.timeline.getWindow();
      const zoom =
        (moment(end).get('minutes') * moment(start).get('minutes')) / 20;
      // const timelineWidth =
      //   document.body.querySelector('.vis-timeline')?.getBoundingClientRect()
      //     ?.width || 0;
      // const timeWidth = moment(end, 'seconds').diff(start, 'seconds');
      const delta = moment(item.start, 'seconds').diff(
        closestItem.end,
        'seconds'
      );

      const distance = delta * zoom;
      if (distance < 20) {
        item.start = closestItem.end as Date;
        item.end = moment(item.start)
          .add(gvgSkillDur + gvgSkillLead, 'seconds')
          .toDate();
      }
    }

    // Keep in timeline range
    const max = moment(timelineRef.current.props?.options?.max)
      .subtract(gvgSkillLead, 'seconds')
      .toDate();
    const min = timelineRef.current.props?.options?.min || 0;
    if (item.start > max) {
      item.start = max;
      item.end = moment(item.start)
        .add(gvgSkillDur + gvgSkillLead, 'seconds')
        .toDate();
    } else if (item.start < min) {
      item.start = timelineRef.current.props.options?.min as number;
      item.end = moment(item.start as number)
        .add(gvgSkillDur + gvgSkillLead, 'seconds')
        .toDate();
    }

    callback(item);
  };

  const handleOnAdd = (item: TimelineItem, callback: any) => {
    if ('nightmare' in item) {
      resetSharingLink();

      return callback(item);
    }

    return callback(null);
  };
  const handleOnRemove = (item: TimelineItem, callback: any) => {
    resetSharingLink();
    return callback(item);
  };

  const options: TimelineOptions = {
    width: '100%',
    minHeight: '150px',
    maxHeight: '400px',
    autoResize: true,
    groupHeightMode: 'fixed',
    editable: true,
    multiselect: true,
    showCurrentTime: false,
    showMajorLabels: false,
    showTooltips: false,
    format: {
      minorLabels: {
        minute: 'mm:ss'
      }
    },
    rollingMode: {},
    onAdd: handleOnAdd,
    onRemove: handleOnRemove,
    onMoving: handleItemMoving,
    onInitialDrawComplete: () => {
      timelineReady = true;
      if (initialItems) {
        setTimeout(() => {
          nightmarePlannerContext.timelineRef?.current?.items.add(
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            initialItems!
          );
          initialItems = undefined;
        }, 0);
      }
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    template: (function (
      item: TimelineItem,
      element: Element,
      data: TimelineDataItem
    ) {
      if (!item || !data || data.type !== 'range') return;

      return ReactDOM.createPortal(
        ReactDOM.render(
          <NightmareItem nightmare={data.nightmare} />,
          element
        ) as ReactNode,
        element
      );
    } as unknown) as TimelineOptionsTemplateFunction,
    snap: null,
    rtl: true,
    type: 'range',
    itemsAlwaysDraggable: true,
    timeAxis: { scale: 'minute', step: 1 },
    min: newMoment().minutes(1).toDate(),
    max: newMoment().minutes(20).toDate(),
    start: newMoment().minutes(1).toDate(),
    end: newMoment().minute(8).toDate(),
    zoomMin: newMoment().minutes(3).valueOf(),
    margin: {
      axis: 0,
      item: {
        horizontal: -1
      }
    }
  };

  const handleItemDrop = (event: TimelineEventPropertiesResult) => {
    const timelineRef = nightmarePlannerContext.timelineRef;
    const nativeEvent = (event.event as unknown) as React.DragEvent<HTMLDivElement>;
    const data = nativeEvent.dataTransfer.getData('text');
    if (!data) return;

    const {
      id,
      nightmare: { gvgSkillLead, gvgSkillDur }
    } = JSON.parse(data) as TimelineDataItem;

    // Get item data
    const itemData = timelineRef?.current?.items.get(id) as TimelineItem;
    if (!itemData) return;

    const end = moment(itemData.start)
      .add(gvgSkillLead + gvgSkillDur, 's')
      .valueOf();

    timelineRef?.current?.items.updateOnly({
      id,
      end,
      start: itemData.start.valueOf()
    });
  };

  return (
    <Scene title="Nightmare Planner">
      <NightmareContext.Provider value={{ nightmares }}>
        {isLoading && <Spinner />}
        {!isLoading && (
          <>
            <MemoizedTimeline
              ref={nightmarePlannerContext.timelineRef}
              options={options}
              dropHandler={handleItemDrop}
              initialGroups={[{ id: 1, content: 'Group 1' }]}
            />
            <NightmareTabs />
          </>
        )}
      </NightmareContext.Provider>
    </Scene>
  );
}

export default NightmarePlanner;
