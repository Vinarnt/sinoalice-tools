import { Base64 } from 'js-base64';

class StateSerializer {
  public serialize<T>(state: T): string {
    return Base64.encode(JSON.stringify(state), true);
  }

  public deserialize<T>(data: string): T {
    return JSON.parse(Base64.decode(data)) as T;
  }
}

export default new StateSerializer();
