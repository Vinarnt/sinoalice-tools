import React, { ReactElement } from 'react';
import { v1 } from 'uuid';
import { NightmareItem as NightmareItemData } from '../../../../services/NightmareService';
import NightmareItem, {
  TimelineDataItem
} from '../NightmareItem/NightmareItem';

import './DraggableNightmareItem.css';

type Props = {
  nightmare: NightmareItemData;
  onItemDrop?: (event: React.DragEvent<HTMLDivElement>) => void;
};

export default function DraggableNightmareItem({
  nightmare
}: Props): ReactElement {
  const handleDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    event.dataTransfer.dropEffect = 'copy';
    event.dataTransfer.effectAllowed = 'copy';

    const id = v1();
    const item: Partial<TimelineDataItem> = {
      id,
      content: nightmare.nameEN || nightmare.name,
      type: 'range',
      nightmare,
      className: 'nightmare-item'
    };

    event.dataTransfer.setData('text', JSON.stringify(item));
  };

  return (
    <div className="draggable" draggable={true} onDragStart={handleDragStart}>
      <NightmareItem nightmare={nightmare} />
    </div>
  );
}
