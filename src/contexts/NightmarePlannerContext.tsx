import React, { createContext, RefObject, useRef } from 'react';
import Timeline from 'react-vis-timeline';

export interface NightmarePlannerContextType {
  timelineRef: RefObject<Timeline> | undefined;
}

const NightmarePlannerContext = createContext<NightmarePlannerContextType>({
  timelineRef: undefined
});

export const NightmarePlannerConsumer = NightmarePlannerContext.Consumer;

type ProviderProps = {
  children: React.ReactNode;
};
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const NightmarePlannerProvider = function ({ children }: ProviderProps) {
  return (
    <NightmarePlannerContext.Provider
      value={{
        timelineRef: useRef<Timeline>() as RefObject<Timeline>
      }}
    >
      {children}
    </NightmarePlannerContext.Provider>
  );
};

export default NightmarePlannerContext;
