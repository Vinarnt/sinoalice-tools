import React, { ReactElement } from 'react';
import { TimelineItem } from 'vis-timeline/types';

import { NightmareItem as NightmareDataItem } from '../../../../services/NightmareService';
import Tooltip from '../../../Tooltip/Tooltip';
import './NightmareItem.css';

type Props = {
  nightmare: NightmareDataItem;
};

export interface TimelineDataItem extends TimelineItem {
  nightmare: NightmareDataItem;
}

export default function NightmareItem({ nightmare }: Props): ReactElement {
  return (
    <div className="nightmare-item-container">
      <img
        data-tip
        data-for={nightmare.id}
        className="nightmare-item-image"
        src={'images/nightmares/' + nightmare.image}
      ></img>
      <Tooltip
        id={`${nightmare.id}`}
        effect="solid"
        clickable={true}
        place="bottom"
      >
        <span className="nightmare-item-tooltip-name">
          {nightmare.nameEN || nightmare.name}
        </span>
        <br />
        <span className="nightmare-item-tooltip-skill-name">
          {nightmare.gvgSkillEN || nightmare.gvgSkill}
        </span>
        <br />
        <span className="nightmare-item-tooltip-label">Preparation time:</span>
        {nightmare.gvgSkillLead}s
        <br />
        <span className="nightmare-item-tooltip-label">Duration:</span>
        {nightmare.gvgSkillDur}s
        <br />
        <span className="nightmare-item-tooltip-label">SP cost:</span>
        {nightmare.gvgSkillSP}
        <br />
        {nightmare.global && (
          <>
            <br />
            <span className="nightmare-item-tooltip-global-info">
              Available in Global version !
            </span>
          </>
        )}
        {!nightmare.global && (
          <>
            <br />
            <span className="nightmare-item-tooltip-global-warn">
              Not available in Global version !
            </span>
          </>
        )}
      </Tooltip>
    </div>
  );
}
