import axios from 'axios';
import fs = require('fs');
import https = require('https');

axios
  .get('https://sinoalice.game-db.tw/package/alice_nightmares_global-en.js')
  .then(({ data }) => {
    const rows: string[] = data['Rows'];
    const cols: string[] = data['Cols'].split('|');

    // Merge columns headers with row data
    const items = rows.map((row: string) => {
      const nightmareData: string[] = row.split('|');
      const item: any = nightmareData.reduce((acc, data, index) => {
        const colName = cols[index];
        if (colName === 'Global') {
          acc[camelize(colName)] = data === '' ? false : Boolean(data);
        } else {
          acc[camelize(colName)] = !isNaN(Number.parseInt(data))
            ? Number(data)
            : data;
        }
        return acc;
      }, {});
      item.image = `CardS${String(item.icon).padStart(4, '0')}.png`;

      // Download image to file
      downloadImage(item.image);

      return item;
    });

    // Write items to file
    fs.writeFile(
      './public/data/nightmares.json',
      JSON.stringify(items),
      (error) => {
        if (error) throw error;
      }
    );
  })
  .catch((e) => {
    console.error('Unable to fetch data');
    console.error(e);
  });

function downloadImage(image: string) {
  const imageFile = fs.createWriteStream(`./public/images/nightmares/${image}`);
  https
    .get(
      `https://sinoalice.game-db.tw/images/card/${image}`,
      function (response) {
        response.pipe(imageFile);
        imageFile.on('finish', function () {
          imageFile.close();
        });
      }
    )
    .on('error', function (error) {
      console.error(error.message);
    });
}

function camelize(str) {
  if (str === 'ID') return 'id';
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
    if (+match === 0) return '';
    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
}
