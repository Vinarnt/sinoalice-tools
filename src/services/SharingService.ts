class SharingService {
  public async getSharingLink(url: string): Promise<string> {
    return (
      await fetch(
        'https://tinyurl.com/api-create.php?url=' + encodeURIComponent(url),
        {
          headers: new Headers({
            'Content-Type': 'application/json'
          })
        }
      )
    ).text();
  }
}

export default new SharingService();
